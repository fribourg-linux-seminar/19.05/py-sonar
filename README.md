# py-sonar

MicroPython demo for the [16th Fribourg Linux Seminar](https://www.meetup.com/fr-FR/Fribourg-Linux-Seminar/events/256776987/) : Sonar for measuring distances and 4 digits, 7-segments display.

![py-sonar photo](https://i.imgur.com/bi5BJns.jpg)
