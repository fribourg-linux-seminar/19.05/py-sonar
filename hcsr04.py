"""
Copyright 2019 Jacques Supcik, HEIA-FR

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import machine
import utime as time

PULSE_WIDTH = const(10) # uS
SPEED_OF_SOUND = const(340) # m/s
MAX_WAIT_COUNT_1 = const(1000)
MAX_WAIT_COUNT_2 = const(500_000)

class HCSR04:

    def __init__(self, trigger_pin="Y11", echo_pin="Y12"):
        self.trigger = machine.Pin(trigger_pin, machine.Pin.OUT)
        self.echo = machine.Pin(echo_pin, machine.Pin.IN)
    
    @micropython.native
    def measure(self):
        self.trigger.on()
        time.sleep_us(PULSE_WIDTH)
        self.trigger.off()
        tester = self.echo.value

        # Busy waiting is not the best solution to precisely measure the pulse
        # width. This is OK for the demo, but DO NOT USE THIS IN PRODUCTION!
        count = 0
        while tester() == 0:
            if count > MAX_WAIT_COUNT_1: return None
            count += 1
        t0 = time.ticks_us()
        count = 0
        while tester() == 1:
            if count > MAX_WAIT_COUNT_2: return None
            count += 1
        t1 = time.ticks_us()
        return time.ticks_diff(t1, t0) * SPEED_OF_SOUND / 2000
