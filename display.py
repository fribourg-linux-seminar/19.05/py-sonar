"""
Copyright 2019 Jacques Supcik, HEIA-FR

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from machine import Pin
from pyb import Timer


class Display:

    digits = [
        Pin("X19", Pin.OUT_OD),
        Pin("X20", Pin.OUT_OD),
        Pin("X21", Pin.OUT_OD),
        Pin("X22", Pin.OUT_OD)
    ]

    sa = Pin("X1", Pin.OUT_PP)
    sb = Pin("X2", Pin.OUT_PP)
    sc = Pin("X3", Pin.OUT_PP)
    sd = Pin("X4", Pin.OUT_PP)
    se = Pin("X5", Pin.OUT_PP)
    sf = Pin("X6", Pin.OUT_PP)
    sg = Pin("X7", Pin.OUT_PP)
    sp = Pin("X8", Pin.OUT_PP)

    segments = {
        '0': [sa, sb, sc, sd, se, sf],
        '1': [sb, sc],
        '2': [sa, sb, sg, se, sd],
        '3': [sa, sb, sg, sc, sd],
        '4': [sf, sg, sb, sc],
        '5': [sa, sf, sg, sc, sd],
        '6': [sa, sf, sg, se, sd, sc],
        '7': [sa, sb, sc],
        '8': [sa, sb, sc, sd, se, sf, sg],
        '9': [sa, sb, sg, sf, sc, sd],
        '-': [sg],
        ' ': [],
        '*': [sa, sb, sc, sd, se, sf, sg, sp]
    }

    @micropython.native
    def refresh(self):
        for i in Display.digits:
            i.high()
        for i in Display.segments['*']:
            i.low()
        for i in self.values[self.cur_digit]:
            i.high()

        Display.digits[self.cur_digit].low()
        self.cur_digit = (self.cur_digit + 1) % self.display_len

    def __init__(self, display_len=4, timer=2, refresh_freq=400):
        self.cur_digit = 0
        self.display_len = display_len
        self.values = [Display.segments[' '] for _ in range(self.display_len)]
        tim = Timer(timer)
        tim.init(freq=refresh_freq)
        tim.callback(lambda t: self.refresh())

    def clear(self):
        self.values = [Display.segments[' '] for _ in range(self.display_len)]

    def invalid(self):
        self.values = [Display.segments['-'] for _ in range(self.display_len)]

    def set_value(self, v):
        if v >= 0:
            s = '{{0:{0}d}}'.format(self.display_len).format(v)
        else:
            s = '-{{0:{0}d}}'.format(self.display_len-1).format(-v)
        if len(s) != self.display_len:
            self.invalid()
        else:
            self.values = [Display.segments[i] for i in s]
