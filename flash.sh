#!/bin/bash

source ./env.sh

for i in *.py; do
  echo "Writing $i"
  ampy put $i
done
