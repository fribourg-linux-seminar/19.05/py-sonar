"""
Copyright 2019 Jacques Supcik, HEIA-FR

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import display
import hcsr04
import utime as time

MAX_DISTANCE = const(4000)
SAMPLES = const(13)


def main():
    disp = display.Display()
    sonar = hcsr04.HCSR04()

    while True:
        measures = [0 for _ in range(SAMPLES)]
        for i in range(SAMPLES):
            measures[i] = sonar.measure()
            time.sleep_ms(50)
        measures.sort()
        dist = measures[SAMPLES // 2]
        if dist is not None and dist <= MAX_DISTANCE:
            disp.set_value(int(dist))
        else:
            disp.invalid()


main()
